<?php declare(strict_types=1);
include_once 'action/load.php';
require 'vendor/autoload.php';

use PHPUnit\Framework\TestCase;

class SymbolTest extends TestCase {

    public function testAddSymbol(): void{

        $obj = new Symbol;
        $obj->openConnection();

        $obj->setEmail('test@gmail.com');
        $obj->setSymbol("ABC");
        $obj->setCompany("TEST");
        $obj->setStartDate("2022-01-01");
        $obj->setEndDate("2022-01-01");
        $obj->setStatus(1);
        $obj->addSymbol();
        $output = $obj->getProcessExecutionStatus();
        $this->assertEquals(1, $output);
        $obj->closeConnection();
    }

    public function testSendEmail(): void{
        try {
            $transport = (new Swift_SmtpTransport('smtp.gmail.com', 465, 'ssl'))
            ->setUsername('xxxxxx') //USE YOUR GMAIL CREDENTIALS HERE
            ->setPassword('xxxxxx');//USE YOUR GMAIL CREDENTIALS HERE
         
            $mailer = new Swift_Mailer($transport);
         
            $message = new Swift_Message();
         
            $message->setSubject('ABC');
         
            $message->setFrom(['xxxxxxx' => 'SENDER']); //sender email here
         
            $message->addTo('xxxxxxxx', 'End User');
         
            $message->setBody('2021-01-01 to 2022-01-01');
         
            $result = $mailer->send($message);
            $this->assertEquals(1, $result);

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}