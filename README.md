# php_exercise_for_xm


## Getting started
You are required to install first 2 libraries which i used 

`composer require --dev phpunit/phpunit ^9`

`composer require "swiftmailer/swiftmailer ^6`

## About Project
Its a very simple one form exercise given by `XMGroup` in whcih I need to submit a form. One of the form field has to be retrieve form an api for which I used `CURL`.


After submitting the form, I called another `API` which was given in the assignment, the basic purpose of that API was to get historical data against the symbol which is submitted using the form and show the result and also create a chart.


Also I need to send email for which I used `swiftmailer`.

To use `swiftmailer` please add your credentials, otherwise it will not work!


# important Note!
- The database used in this project is mysql. You can check the database by going in database folder.
- For queries, I used **Prepared Statements**.
- ScreenShot of email is added in `assets/screenshots/` folder. You can check it, email is working

If you have any queries, you can ping me on **rehanraja25@gmail.com**
