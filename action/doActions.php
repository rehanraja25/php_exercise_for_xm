<?php
include_once '../settings.php';
include_once 'common.php';

$objSymbol = new Symbol;
$objCon = $objSymbol->openConnection();

$sTask = (isset($_POST['task']) && !empty($_POST['task'])) ? $_POST['task'] : '';
$sSymbol = (isset($_POST['symbol']) && !empty($_POST['symbol'])) ? $_POST['symbol'] : '';
$sCompany = (isset($_POST['company']) && !empty($_POST['company'])) ? $_POST['company'] : '';
$sEmail = (isset($_POST['email']) && !empty($_POST['email'])) ? $_POST['email'] : '';
$sStarDate = (isset($_POST['start_date']) && !empty($_POST['start_date'])) ? $_POST['start_date'] : '';
$sEndDate = (isset($_POST['end_date']) && !empty($_POST['end_date'])) ? $_POST['end_date'] : '';

if(!empty($sTask)) {
	switch ($sTask){

        case CASE_ADD_SYMBOL:
            if(empty($sSymbol))
                $aAPIResponse[INDEX_MESSAGE] = "Symbol is Missing";
            elseif(empty($sCompany))
                $aAPIResponse[INDEX_MESSAGE] = "Company Name is Missing";
            elseif(empty($sEmail))
                $aAPIResponse[INDEX_MESSAGE] = "Email is Missing";
            elseif(empty($sStarDate))
                $aAPIResponse[INDEX_MESSAGE] = "Start Date is Missing";
            elseif(empty($sEndDate))
                $aAPIResponse[INDEX_MESSAGE] = "End Date is Missing";
            else{
                $objSymbol->setSymbol($sSymbol);
                $objSymbol->setCompany($sCompany);
                $objSymbol->setEmail($sEmail);
                $objSymbol->setStartDate($sStarDate);
                $objSymbol->setEndDate($sEndDate);
                $objSymbol->setStatus(OBJECT_STATUS_ACTIVE);
                $objSymbol->addSymbol();
                if ($objSymbol->getProcessExecutionStatus() == PhpExercise::PROCESS_SUCCESS) {
                    $lastInsertedID = $objSymbol->getLastInsertID();
                   
                    //send email
                    $result = sendEmail($sCompany, $sStarDate, $sEndDate, $sEmail);
                    if ($result) {
                        $aAPIResponse[INDEX_RESCODE] = API_SUCCESS_CODE;
                        $aAPIResponse[INDEX_MESSAGE] = "Symbol Added Successfully";
                        $aAPIResponse[INDEX_DATA] = $lastInsertedID;
                    } else {
                        $aAPIResponse[INDEX_RESCODE] = API_FAILED_CODE;
                        $aAPIResponse[INDEX_MESSAGE] = "Symbol Added But Email Failed";
                    }   
                }
                else{
                    $aAPIResponse[INDEX_RESCODE] = API_FAILED_CODE;
	                $aAPIResponse[INDEX_MESSAGE] = "Symbol Failed To Add";
                }
            }
        break;
    }
}
$objSymbol->closeConnection();
echo json_encode($aAPIResponse);
