<?php
include_once 'appConstant.php';
require '../vendor/autoload.php';

function sendEmail($companyName, $startDate, $endDate, $sendTo){
    try {
        $transport = (new Swift_SmtpTransport('smtp.gmail.com', 465, 'ssl'))
        ->setUsername(MAIL_USER_NAME) //USE YOUR GMAIL CREDENTIALS HERE
        ->setPassword(MAIL_USER_PASSWORD);//USE YOUR GMAIL CREDENTIALS HERE
     
        $mailer = new Swift_Mailer($transport);
        $message = new Swift_Message();
        $message->setSubject($companyName);
        $message->setFrom([MAIL_SENDER_MAIL => $companyName]); //sender email here
        $message->addTo($sendTo, 'End User');
        $message->setBody($startDate . " to " . $endDate);
        $result = $mailer->send($message);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
    if($result)
        return 1;
    else
        return 0;
}