<?php

if (!defined("THISSERVER"))
    define("THISSERVER", $_SERVER["SERVER_NAME"]);

$applicationPath = "/php_exercise";  // don't need last slash.

define("SITEURL", "http://" . $_SERVER['HTTP_HOST'] . $applicationPath . "/");
define("DIR_SEPERATOR", '/');
define("PAGE_EXTENSION", ".php");
define("LINK_TO_HOME", SITEURL);

define("INDEX_RESCODE", "rescode");
define("INDEX_DATA", "data");
define("INDEX_MESSAGE", "message");
define("INDEX_EXTRA", "extra");

define("API_SUCCESS_CODE", 1);
define("API_FAILED_CODE", 0);
define("API_SUCCESS_MESSAGE", "Success");
define("API_FAILED_MESSAGE", "Failed");

define("OBJECT_STATUS_ACTIVE", 1);
define("OBJECT_STATUS_SUSPENDED", 2);
define("OBJECT_STATUS_PENDING", 3);
define("OBJECT_STATUS_DELETED", 4);

define("MAIL_USER_NAME", "xxxxxxxx");
define("MAIL_USER_PASSWORD", "xxxxxxxx");
define("MAIL_SENDER_MAIL", "xxxxxxxx");


define("LINK_TO_DOACTIONS", LINK_TO_HOME . "action" . DIR_SEPERATOR . "doActions" . PAGE_EXTENSION);

//API CASES

define("CASE_ADD_SYMBOL", "CASE_ADD_SYMBOL");