<?php

class Symbol extends Connection {

    //setter
    public function setSymbol($val){
        $this->symbol = $val;
    }

    public function setCompany($val){
        $this->company = $val;
    }

    public function setEmail($val){
        $this->email = $val;
    }

    public function setStartDate($val){
        $this->start_date = $val;
    }

    public function setEndDate($val){
        $this->end_date = $val;
    }

    public function setSymbolID($val){
        $this->symbol_id = $val;
    }

    //getter

    public function getSymbol(){
        return $this->symbol;
    }

    public function getCompany(){
        return $this->company;
    }

    public function getEmail(){
        return $this->email;
    }

    public function getStartDate(){
        return $this->start_date;
    }

    public function getEndDate(){
        return $this->end_date;
    }

    public function getSymbolID(){
        return $this->symbol_id;
    }

    public function addSymbol(){
        
        $email =  $this->getEmail();
        $company = $this->getCompany();
        $symbol = $this->getSymbol();
        $startDate = $this->getStartDate();
        $endDate = $this->getEndDate();
        $status = $this->getStatus();
        $createdAt = $this->nowDTM;

        $this->setProcessExecutionStatus(PhpExercise::PROCESS_FAILED);

        $sQuery = "INSERT INTO `" . PhpExercise::TBL_SYMBOL . "`(" . chr(10);
        $sQuery .= "email, company_name, symbol, start_date, end_date, status, created_at" . chr(10);
        $sQuery .= ") VALUES (" . chr(10);
        $sQuery .= "?, ?, ?, ?, ?, ?, ?" . chr(10);
        $sQuery .= ")";

        try {
            $vConn = $this->getConnection();
            $vStatement = $vConn->prepare($sQuery);
            $vStatement->bind_param("sssssis",
                $email,
                $company,
                $symbol,
                $startDate,
                $endDate,
                $status,
                $createdAt,
            );
            $vStatement->execute();
            $this->setLastInsertID($vConn->insert_id);
            $vStatement->close();
            if($this->getLastInsertID() > 0)
                $this->setProcessExecutionStatus(PhpExercise::PROCESS_SUCCESS);
        } catch (Exception $excepLocInsert) {
            trigger_error("Add Symbol failed with Error: " . $excepLocInsert->getMessage() . " (" . $excepLocInsert->getCode() . ")", E_USER_ERROR);
            $this->setProcessExecutionStatus(PhpExercise::PROCESS_FAILED);
        }
    }
}