<?php
class Connection extends PhpExercise {

	private $_host = "";
	private $_db = "";
	private $_dbUser = "";
	private $_dbPass = "";

	private static $_instance; //The single instance
	private $_connection = null;

	function __construct() {
		$this->_host = "localhost";
		$this->_db = "php_exercise";
        $this->_dbUser = "root";
		$this->_dbPass = "";
	}

	function __destruct() {
		//		$this->closeConnection();
	}

	/**
	 Get an instance of the Database
	 @return Instance
	 */
	public static function getInstance() {
		if(!self::$_instance) { // If no instance then make one
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	public function setConnection($objConnection) {
		$this->_connection = $objConnection;
	}

	public function getConnection() {
		return $this->_connection;
	}

	public function openConnection() {
		$shost = ""; $iUser = ""; $sPass = ""; $sdb = "";
		$shost = $this->_host;
		$iUser = $this->_dbUser;
		$sPass = $this->_dbPass;
		$sdb = $this->_db;
		try{
			$this->_connection = new mysqli($shost, $iUser, $sPass, $sdb);
			if(mysqli_connect_error()) {
				trigger_error("DATABASE CONNECTION FAILED: " . mysqli_connect_error(), E_USER_ERROR);
			}
            /* change character set to utf8 */
            if (!$this->_connection->set_charset("utf8")) {
                trigger_error("Error loading character set utf8: ".$this->_connection->error."\n", E_USER_ERROR);
                exit();
            }
            //$this->_connection->query("SET NAMES utf8");
            $this->_connection->query("SET SESSION sql_mode = ''");

			return $this->_connection;
		}
		catch (Exception $e) {
			$this->throwException($e);
		}
	}


	public function closeConnection() {
		if(!empty($this->_connection))	mysqli_close($this->_connection);
	}

	private function throwException($e) {
		die('Exception : ' . $e->getMessage());
	}
}

?>
