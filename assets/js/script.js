$('#expandBtn').click(function(){
	
	var url = window.location.href + '&fullscreen';
	window.open(url);
    return false;
});

$('#sidePaneToggle').click(function(){
	var el = $(this);
	el.toggleClass('closed');
	
	if (el.hasClass('closed')) {
		$('#sidePane').animate({'left':'-252px'});
		$('#mapWrapper').animate({'left':'9px'});
		$('#svWrapper').animate({'left':'9px'});
	} else {
		$('#sidePane').animate({'left':'0px'});
		$('#mapWrapper').animate({'left':'260px'});
		$('#svWrapper').animate({'left':'260px'});
	}	
	setTimeout(function(){
		google.maps.event.trigger(map, "resize");
		google.maps.event.trigger(panorama, "resize");
	},500)
	
	return false;
});


$('#resultList').on('mouseleave','li', function(){
	var index = $(this).attr('rel');
	var timeoutId = setTimeout( function(){stopAnimated(index)}, 200);
	$(this).data('timeoutId', timeoutId); 

});



$('#sidePane').on('click','a.toggle-btn',function(){
	var el = $(this);
	var container = el.parents('ul'); 
	
	if (el.hasClass('disabled')) {
		
		return false;
	}
	
	if (! el.hasClass('toggle')) {
	
		
		if(defaultSearchCase || queryNotFoundCase) {

	
			container.find('li a.toggle-btn').removeClass('toggle').text(lang.btnSV);
			container.off('click','li', listItmeClick);
			
			var lat = el.siblings('p.latitude').text();
			var lng = el.siblings('p.longitude').text();						
			panorama.setPosition(new google.maps.LatLng(lat,lng));
			
			
		}
		
		sv.getPanoramaByLocation(panorama.getPosition(), 50, checkSVAvailable);
		
		el.toggleClass('toggle');
		el.text(lang.btnMV);

	
	} else {
		
		el.toggleClass('toggle');
		el.text(lang.btnSV);
		
		$('#svWrapper').fadeOut(500,function(){
			panorama.setVisible(false);
		});
		
		if(defaultSearchCase || queryNotFoundCase) {
			container.on('click','li', listItmeClick);
		}
		
	}
	
	return false;
});
