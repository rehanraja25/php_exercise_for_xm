function productsDropDown() {
    $(".products_dropdown").toggleClass("hide");
    if ($('#support_dropdown').is(':visible'))
        $(".support_dropdown").toggleClass("hide");

    if ($('#complaint_dropdown').is(':visible'))
        $(".complaint_dropdown").toggleClass("hide");


}

function openMobileMenu() {
    $(".mobile_menu_list").toggleClass("hide");
}

function openProductsMobile() {
    $(".productsdrop_mbl").toggleClass("hide");
}

function supportDropDown() {
    $(".support_dropdown").toggleClass("hide");
    if ($('#products_dropdown').is(':visible'))
        $(".products_dropdown").toggleClass("hide");

    if ($('#complaint_dropdown').is(':visible'))
        $(".complaint_dropdown").toggleClass("hide");
}

function complaintsDropDown() {
    $(".complaint_dropdown").toggleClass("hide");
    if ($('#products_dropdown').is(':visible'))
        $(".products_dropdown").toggleClass("hide");

    if ($('#support_dropdown').is(':visible'))
        $(".support_dropdown").toggleClass("hide");
}

function opensupportMobile() {
    $(".supportdrop_mbl").toggleClass("hide");
}

function opencomplaintMobile() {
    $(".complaintdrop_mbl").toggleClass("hide");
}