<?php include_once "../shared/_head.php";

$symbol = (isset($_GET['symbol']) && !empty($_GET['symbol'])) ? $_GET['symbol'] : '';

$curl = curl_init();

curl_setopt_array($curl, [
	CURLOPT_URL => "https://yh-finance.p.rapidapi.com/stock/v3/get-historical-data?symbol=$symbol&region=US",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => [
		"X-RapidAPI-Host: yh-finance.p.rapidapi.com",
		"X-RapidAPI-Key: 1b212f4a5bmsh96e6a61211af5fcp1315f6jsnb2ccb63efcf4"
	],
]);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	echo "cURL Error #:" . $err;
} else {
    $output = $response;
}

?>

<body>
    <div class="container">
        <br />
		<br />
		<br />
		<br />
		<br/>
		<h2 align="center">Historical Data</h2>
		<br/>
		<br/>
		<div id="response-div"></div>
		<br />

        <table id="historical_data" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Open</th>
                    <th>High</th>
                    <th>Close</th>
                    <th>Volume</th>
                </tr>
            </thead>
			<tbody>
			</tbody>
        </table>
		<br />
		<br />
		<br />
		<br />
		<div>
			<canvas id="myChart"></canvas>
		</div>
		<br />
		<br />
		<br />
		<br />
    </div>
</body>
</html>

<script>
	$(document).ready(function () {
		var response = '<?php echo $output; ?>';
		response = JSON.parse(response);
		response = response.prices;
		console.log(response);

		if(response.length == 0)
			$("#response-div").html("<div class='alert alert-danger'> No DataAvailable against this Symbol</div>");
		else{
			let dataset1 = [];
			let dataset2 = [];
			let count = [];

			var dt = $('#historical_data').DataTable();


			$.each(response, function (index, data){
				if(!data.open)
					data.open = 0;
				if(!data.high)
					data.high = 0;
				if(!data.close)
					data.close = 0;
				if(!data.volume)
					data.volume = 0;
				if(!data.date)
					data.date = 0;

				dt.row.add([
					data.date, data.open, data.high, data.close, data.volume
				]).draw();

				dataset1.push(data.open);
				dataset2.push(data.close);
				count.push(index);
			});

			const data = {
				labels: count,
				datasets: [
					{
						label: 'Open',
						backgroundColor: 'rgb(255, 99, 132)',
						borderColor: 'rgb(255, 99, 132)',
						data: dataset1,
					},
					{
						label: 'Close',
						backgroundColor: 'rgb(0, 255, 255)',
						borderColor: 'rgb(0, 255, 255)',
						data: dataset2,
					}
				]
			};

			const config = {
				type: 'line',
				data: data,
				options: {}
			};

			const myChart = new Chart(document.getElementById('myChart'),config);
		}
    });
</script>