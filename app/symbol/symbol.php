<?php include_once "../shared/_head.php";

$curl = curl_init();

curl_setopt_array($curl, [
	CURLOPT_URL => "https://pkgstore.datahub.io/core/nasdaq-listings/nasdaq-listed_json/data/a5bc7580d6176d60ac0b2142ca8d7df6/nasdaq-listed_json.json",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",

]);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	echo "cURL Error #:" . $err;
}
else{
    $output = '';
    $response = json_decode($response, true);
    foreach($response as $row){
        $output .= '<option value="'.$row["Company Name"].'">'.$row["Symbol"].'</option>';
    }
}

?>


<body>
    <div class="container">
        <br/>
		<h2 align="center">PHP Exercise</h2>
		<br />
		<br />
		<br />
		<br />

		<div id="response-div"></div>

		<div class="panel panel-default">
			<div class="panel-body">
				<form id="symbol_add" method="post" autocomplete="off">
					<div class="form-group">
						<select class="form-control" id ="symbol" required>
                            <option disabled selected value>Please Select Symbol</option>
                            <?php echo $output; ?>
                        </select>
					</div>
					<div class="form-group">
						<input type="email" name="email" id="email" class="form-control" placeholder="Enter Email" required />
					</div>

                    <div class="row">
                        <div class="col-md-6">
                            Start Date:
                            <br>
                            <input id="start_date" name="start_date" width="100%" class="form-control" required/>

                        </div>

                        <div class="col-md-6">
                            End Date:
                            <br>
                            <input id="end_date" name="end_date" width="100%" class="form-control" required/>
                        </div>


		            </div>
                    <br />
                    <br />

					<div class="form-group">
						<input type="submit" value="Submit" class="btn btn-info" />
					</div>
				</form>
				<div class="col-md-2 d-none" id="loader">
					<img src="../../assets/images/loader.gif" class="mr-lt" />
				</div>
			</div>
		</div>
	</div>
</body>

<script>

	$('#start_date').datepicker({
		dateFormat: 'yy-mm-dd',
		uiLibrary: 'bootstrap4',
		changeMonth: false,
    	changeYear: false,
   	 	//stepMonths: false,
		maxDate: new Date(new Date().setDate(new Date().getDate())),
		onSelect: function(selected) {
			var date = $(this).datepicker("getDate");
			var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).getTime();

			if( today > date){
				var nextDay = new Date(selected);
				nextDay.setDate(nextDay.getDate() + new Date(new Date().setDate(new Date().getDate())));

				$("#end_date").datepicker( "option", "minDate", selected );
				if(nextDay < today)
					$("#end_date").datepicker( "option", "maxDate", nextDay);
				else
					$("#end_date").datepicker( "option", "maxDate", new Date(new Date().setDate(new Date().getDate())));
			}
			else{
				$("#end_date").datepicker( "option", "minDate", selected );
				$("#end_date").datepicker( "option", "maxDate", new Date(new Date().setDate(new Date().getDate())));
			}
        }
	});
	
	$('#end_date').datepicker({
		dateFormat: 'yy-mm-dd',
		uiLibrary: 'bootstrap4',
		changeMonth: false,
    	changeYear: false,
   	 	//stepMonths: false,
	});


	$('#symbol_add').submit(function(e) {
		e.preventDefault();

		$("#response-div").html('');
		$("#loader").removeClass("d-none");

		var company = $('#symbol').val();
		var symbol = $('#symbol').find("option:selected").text();
		var email = $('#email').val();
		var startDate = $('#start_date').val();
		var endDate = $('#end_date').val();

		//alert('<?php echo LINK_TO_DOACTIONS; ?>');

		$.ajax({
			url: '<?php echo LINK_TO_DOACTIONS; ?>',
			method: "POST",
			data: {
				"task": '<?php echo CASE_ADD_SYMBOL; ?>',
				"symbol": symbol,
				"company": company,
				"email": email,
				"start_date": startDate,
				"end_date": endDate
			},
			cache: false,
			success: function(response) {
				$("#loader").addClass("d-none");
				var response = JSON.parse(response);
				if(response.rescode == 1){
					$("#response-div").html("<div class='alert alert-success'>" + response.message + "</div>");
					setTimeout(()=> {
						window.location.href = "historical_data.php?symbol="+symbol;
					}, 2000);
				}
				else{
					$("#response-div").html("<div class='alert alert-danger'>" + response.message + "</div>");
				}
			}
		});
	});

</script>